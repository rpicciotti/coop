var app = angular.module("WowApp", []);

app.controller("WowController", function($scope, $http) {
  /*$http.get("https://us.api.battle.net/wow/character/Dalaran/Regex?locale=en_US&apikey=wkt3rzuvc8pwdxnuf55hkdeyrhetycry")
  .success(function(response){
      alert(respnse);
  }*/

  /*
   * Gets character details off of battle.net API
   *
   */
  $scope.searchCharacter = function(){
    var characterName = $scope.searchByCharacter;
    var realm = $scope.searchByRealm;
    $http({
    method: 'GET',
    url: 'https://us.api.battle.net/wow/character/' + realm + '/'+ characterName + '?fields=stats&locale=en_US&apikey=wkt3rzuvc8pwdxnuf55hkdeyrhetycry'
  }).then(function successCallback(response) { //Gets a valid response
      $scope.character = {
        name: response.data.name,
        level: response.data.level,
        health: response.data.stats.health,
        str: response.data.stats.str,
        agi: response.data.stats.agi,
        int: response.data.stats.int,
        sta: response.data.stats.sta,
        armor: response.data.stats.armor,
        dodge: response.data.stats.dodge,
        parry: response.data.stats.parry,
        block: response.data.stats.block,

        //Questionable
        dmg_min: response.data.stats.mainHandDmgMin,
        dmg_max: response.data.stats.mainHandDmgMax,
        speed: response.data.stats.mainHandSpeed,
        //End Questionable

        crit: response.data.stats.crit,
        haste: response.data.stats.haste,
        mastery: response.data.stats.mastery,
        leech: response.data.stats.leech,

        //Questionable
        mana5: response.data.stats.mana5
        //End Questionable
      };
      $scope.searchItems();
    }, function errorCallback(response) {
      // If an error occurs
    });
  }

  $scope.searchItems = function(){

    //character details
    var characterName = $scope.searchByCharacter;
    var realm = $scope.searchByRealm;

    /**
      * Get the list of items from each character, we need the IDs for each item
      **/
    $http({
      method: 'GET',
      url: 'https://us.api.battle.net/wow/character/' + realm + '/' + characterName + '?fields=items&locale=en_US&apikey=wkt3rzuvc8pwdxnuf55hkdeyrhetycry'
    }).then(function successCallback(response) { //Gets a valid response

      //items has the id, itemsdata will have the needed information about items
      $scope.items = response.data.items;
      $scope.itemsdata = [];

      //Loop goes through each item the character is wearing
      angular.forEach($scope.items, function(value, key) {

        //if statement ignores metadata
        if (!angular.isUndefined(value.id)){
          $http({ //Gets the item data given the id
            method: 'GET',
            url: 'https://us.api.battle.net/wow/item/' + value.id + '?locale=en_US&apikey=wkt3rzuvc8pwdxnuf55hkdeyrhetycry'
          }).then(function successCallback(response) { //Gets a valid response

            //Create an entry for the itemdata list
            nextItem = {
                name: response.data.name,
                buyPrice: response.data.buyPrice,
                sellPrice: response.data.sellPrice,
                requiredLevel: response.data.requiredLevel
              };

              $scope.itemsdata.push(nextItem); //append data to list
          }, function errorCallback(response) {
            // If an error occurs
          });
        }
      })

    }, function errorCallback(response) {
      // If an error occurs
    });
  }


});
